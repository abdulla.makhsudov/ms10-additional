package az.ingress.ms10demo1.controller.dto;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {

    private Long age;

    private String firstName;

    private String lastName;

    private String school;

    private String homeAddress;

    private String mobile;

    private String email;


    public static void main(String[] args) {
        StudentDto studentDto = StudentDto
                .builder()
                .age(10L)
                .firstName("test")
                .email("eamil@test.az")
                .build();
    }
}
