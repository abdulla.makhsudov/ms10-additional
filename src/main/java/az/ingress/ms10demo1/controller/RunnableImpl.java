package az.ingress.ms10demo1.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Scope("prototype")
@Component
public class RunnableImpl implements Runnable {

    private int speed;

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public void run() {
        System.out.println("Running...");
    }
}
