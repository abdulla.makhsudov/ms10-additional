package az.ingress.ms10demo1.controller;

import az.ingress.ms10demo1.controller.dto.StudentDto;
import az.ingress.ms10demo1.domain.Student;
import az.ingress.ms10demo1.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students") //plarual
public class StudentController {

    private final StudentService service;


    @GetMapping("/{id}") // GET
    public Student getStudent(@PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping //POST
    @ResponseStatus(HttpStatus.CREATED)
    public Student createStudent(@RequestBody Student student) {
        return service.createStudent(student);
    }

    @PutMapping("/{id}") //PUT
    public Student updateStudent(@PathVariable Long id, @RequestBody Student student) {
        return service.updateStudent(id, student);
    }

    @DeleteMapping("/{id}") //DELETE
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id) {
        service.deleteStudent(id);
    }

    @GetMapping("/list")
    public List<Student> getStudent() {
        return service.getStudentList();
    }
}
