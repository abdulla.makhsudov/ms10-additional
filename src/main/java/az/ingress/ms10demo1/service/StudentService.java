package az.ingress.ms10demo1.service;

import az.ingress.ms10demo1.domain.Student;

import java.util.List;

public interface StudentService {

    Student getById(Long id);

    Student createStudent(Student student);

    Student updateStudent(Long id, Student student);

    void deleteStudent(Long id);

    List<Student> getStudentList();
}
