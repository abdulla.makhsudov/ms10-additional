package az.ingress.ms10demo1.service;

import az.ingress.ms10demo1.dto.CarDTO;

import java.util.List;

public interface CarService {

    List<CarDTO> getCars();

    CarDTO getCarById(Long id);

    CarDTO createCar(CarDTO carDTO);

    CarDTO updateCar(Long id, CarDTO carDTO);

    void deleteCar(Long id);
}
