package az.ingress.ms10demo1.service;

import az.ingress.ms10demo1.domain.Car;
import az.ingress.ms10demo1.dto.CarDTO;
import az.ingress.ms10demo1.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;

    public List<CarDTO> getCars() {
        List<Car> carList = carRepository.findAll();
        return carList.stream().map(car -> modelMapper.map(car, CarDTO.class)).collect(Collectors.toList());
    }

    @Override
    public CarDTO getCarById(Long id) {
        Car car = getCarByIdMethod(id);
        return modelMapper.map(car, CarDTO.class);
    }

    @Override
    public CarDTO createCar(CarDTO carDTO) {
        Car createdCar = carRepository.save(modelMapper.map(carDTO, Car.class));
        return modelMapper.map(createdCar, CarDTO.class);
    }

    @Override
    public CarDTO updateCar(Long id, CarDTO carDTO) {
        Car car = getCarByIdMethod(id);
        car.setMarka(carDTO.getMarka());
        car.setModel(carDTO.getModel());
        car.setYear(carDTO.getYear());
        Car updatedCar = carRepository.save(car);
        return modelMapper.map(updatedCar, CarDTO.class);
    }

    @Override
    public void deleteCar(Long id) {
        getCarByIdMethod(id);
        carRepository.deleteById(id);
    }

    private Car getCarByIdMethod(Long id) {
        return carRepository.findById(id).orElseThrow(() -> new ResolutionException("Car not found"));
    }
}
