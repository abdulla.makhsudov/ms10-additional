package az.ingress.ms10demo1.config;

import az.ingress.ms10demo1.controller.Runnable;
import az.ingress.ms10demo1.controller.RunnableImpl;
import az.ingress.ms10demo1.controller.RunnableImpl2;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

@Configuration
public class Config {

//    @Bean
//    @Primary
//    public Runnable runnable() {
//        System.out.println("Runnable initializing...");
//        RunnableImpl runnable = new RunnableImpl();
//        System.out.println("Bean id is " + runnable);
//        return runnable;
//    }
//
//    @Bean
//    public Runnable myRunnable() {
//        System.out.println("Runnable 2 initializing...");
//        RunnableImpl2 runnable = new RunnableImpl2();
//        System.out.println("Bean id  2 is " + runnable);
//        return runnable;
//    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
