package az.ingress.ms10demo1.builder;

public class Main {

    public static void main(String[] args) {
        Car bmw = Car.Builder.getInstance("bmw", "328")
                .id(2L)
                .year(1999)
                .build();
        System.out.println(bmw);
    }
}
