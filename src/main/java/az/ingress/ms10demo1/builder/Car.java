package az.ingress.ms10demo1.builder;


public final class Car {

    private Long id;
    private String marka; // required
    private String model; // required
    private int year;

    private Car(Builder builder) {
        this.id = builder.id;
        this.marka = builder.marka;
        this.model = builder.model;
        this.year = builder.year;
    }

    public Long getId() {
        return id;
    }

    public String getMarka() {
        return marka;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }

    public static class Builder {
        private Long id;
        private String marka;
        private String model;
        private int year;

        private Builder() {}

        public Builder(String marka, String model) {
            this.marka = marka;
            this.model = model;
        }

        public static Builder getInstance(String marka, String model) {
            return new Builder(marka, model);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder year(int year) {
            this.year = year;
            return this;
        }

        public Car build() {
            Car car = new Car(this);
            return car;
        }
    }
}
