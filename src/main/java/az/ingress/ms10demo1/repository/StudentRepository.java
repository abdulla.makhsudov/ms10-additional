package az.ingress.ms10demo1.repository;

import az.ingress.ms10demo1.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
